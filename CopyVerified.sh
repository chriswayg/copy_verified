#!/bin/bash
#    'CopyVerified' (or 'CopyScript') - Copy a folder to up to four destinations and verify exact copy using checksums
#    Copyright (C) 2011,2012  Dustin Cross, modifications and GUI by Christian Wagner
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# History / Changelog:
#
#  01/09/2011
# This script makes a copy of a directory (folder) to up to four destinations
# It then makes an md5 checksum of the source and each destinations
# finally it compares those md checksums to ensure your copies match exactly
#
# This script makes the first copy and then all secondary copies from that copy
# So you should make your first destination to your fastest drive
#
# Dustin Cross 
#
#
#  02/02/2011
# Modified the script so md5 checks work with copied subdirectories.
#
#  02/02/2011
# changed script so errors print to the screen instead of to /dev/null
#
#  02/10/2011
# cleaned up the code a little
#
#  02/17/11
# fixed a problem where the copy didn't work on files with "." in the filename
#
#  02/17/11
# made the md5 part ignore folders so it doesn't give errors on folders.
# md5 now only works on files and not directories or any special files
#
#  02/26/11
# added percent complete
#
# 03/11/11
# Cleaned up things a little.  Improved performance a little by having source
# checksum happen during second copy.  Also made errors write to a log file.
# Will add more for this log file in the future.
#
# 04/19/11
# Added details to the log file
# Modified the script so you can run multiple copies and still have clean log files
#
# 04/22/11
# Fixed the script so it tells you when Source Checksums are complete.
#
# 04/23/11
# added view log after copy
# cleaned up the log file a little
#
# 07/01/11
# changed the order of the questions at teh beginning.
#
##still to do ##
# make completion statement based on output of md5 success or fail
# make -v tell version
# add summary to log file (copied x files to x destionaltion)
# add summary to log file (copied x MB in X secs)
# check website for new version at launch
#   -add copyscript folder to Sandust with each new version
#   -have script offer to download new version and install
#
#
# 01/21/12 modifications and GUI by Christian Wagner
# - added a Pashua GUI frontend for inputting the file paths and data
# - added Platypus wrapper for making it an app and for terminal -like output
# - added support for spaces in path and file names
# - added the -n option to cp to prevent from accidentally overwriting files
# - added sanity check for validity of paths
# - chanage MD5 files from .txt to .md5 to facilitate verfication with other tools
# - renamed to have a name that is less generic
# 01/22/12
# - configurable file name extension for MD5 files (.txt or .md5)
# - added GPL license
# 01/23/12
# - uses the unmodified Pashua.app again; renaming it was not needed, as it is hidden anyways.
# - added GUI loop until user exits by clicking Cancel
# - added GUI loop until all user input is OK
# - determine the intention of the user as to the number of copies without a dropdown box
#
VERSION="version 20120123.1 beta"
#
# Wrapper function for interfacing to Pashua. Written by Carsten
# Bluem <carsten@bluem.net> in 10/2003, modified in 12/2003 (including
# a code snippet contributed by Tor Sigurdsson), 08/2004 and 12/2004.

# to avoid the 'TERM environment variable not set' error
TERM=dumb
export TERM

# Choose preferred file name extension for md5 files: .md5 or _md5.txt
MD5TXT=".md5"

pashua_run() {

	# Write config file
	pashua_configfile=`/usr/bin/mktemp /tmp/pashua_XXXXXXXXX`
	echo "$1" > $pashua_configfile

	# Find Pashua binary. We do search both . and dirname "$0"
	# , as in a doubleclickable application, cwd is /
	# BTW, all these quotes below are necessary to handle paths
	# containing spaces.
	bundlepath="Pashua.app/Contents/MacOS/Pashua"
	if [ "$3" = "" ]
	then
		mypath=`dirname "$0"`
		for searchpath in "$mypath/Pashua" "$mypath/$bundlepath" "./$bundlepath" \
						  "/Applications/$bundlepath" "$HOME/Applications/$bundlepath"
		do
			if [ -f "$searchpath" -a -x "$searchpath" ]
			then
				pashuapath=$searchpath
				break
			fi
		done
	else
		# Directory given as argument
		pashuapath="$3/$bundlepath"
	fi

	if [ ! "$pashuapath" ]
	then
		echo "Error: Pashua could not be found"
		exit 1
	fi

	# Manage encoding
	if [ "$2" = "" ]
	then
		encoding=""
	else
		encoding="-e $2"
	fi

	# Get result
	result=`"$pashuapath" $encoding $pashua_configfile | sed 's/ /;;;/g'`

	# Remove config file
	rm $pashua_configfile

	# Parse result
	for line in $result
	do
		key=`echo $line | sed 's/^\([^=]*\)=.*$/\1/'`
		value=`echo $line | sed 's/^[^=]*=\(.*\)$/\1/' | sed 's/;;;/ /g'`
		varname=$key
		varvalue="$value"
		eval $varname='$varvalue'
	done

}

# This fuction saves the settings to config files and calls the copy functions
saveconfig_thencopy ()
	{
	if [ $NUM_COPIES -eq 1 ]
	then
		echo " " ;
		echo "$NUM_COPIES, $COPYPATH1" > ~/Library/Logs/CopyVerified/copyverified.config ;
		echo $DMNAME > ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config ;
        # the first version of this gets truncated, if the path has spaces
#		DESTPATH=`echo $SOURCEPATH | awk -F\/ '{print $NF}' | awk '{print $1}'` ;
		DESTPATH=`echo "$SOURCEPATH" | awk -F\/ '{print $NF}'` ;
		onecopy 
	else
		if [ $NUM_COPIES -eq 2 ]
		then
			echo " " ;
			echo "$NUM_COPIES, $COPYPATH1, $COPYPATH2" > ~/Library/Logs/CopyVerified/copyverified.config ;
			echo $DMNAME > ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config ;
			DESTPATH=`echo "$SOURCEPATH" | awk -F\/ '{print $NF}'` ;
			twocopy 
		else 
			if [ $NUM_COPIES -eq 3 ]
			then
				echo " " ;
				echo "$NUM_COPIES, $COPYPATH1, $COPYPATH2, $COPYPATH3" > ~/Library/Logs/CopyVerified/copyverified.config ;
				echo $DMNAME > ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config ;
				DESTPATH=`echo "$SOURCEPATH" | awk -F\/ '{print $NF}'` ;
				threecopy 
			else 
				if [ $NUM_COPIES -eq 4 ]
				then
					echo " " ;
					echo "$NUM_COPIES, $COPYPATH1, $COPYPATH2, $COPYPATH3, $COPYPATH4" > ~/Library/Logs/CopyVerified/copyverified.config ;
					echo $DMNAME > ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config ;
					DESTPATH=`echo "$SOURCEPATH" | awk -F\/ '{print $NF}'` ;
					fourcopy
				fi
			fi
		fi
	fi
	}

# This part of the script defines the sequence of events
onecopy ()
	{
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo `date` >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " Data Manager: $DMNAME" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "       Source: $SOURCEPATH" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 1: $COPYPATH1" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "----------------------------------------------------------------------" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	date
	copy1
	check_source &
	check_dest1 &
	check_dest1_comp
	wait
	compa1
	echo
	echo TRANSFERS and MD5 CHECKSUMS COMPLETE
	date
	echo "`date | awk '{print $4}'` Copies and Checksums Sucessful" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH1"/"$DESTPATH"-copy1_log.txt
	cat ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log >> ~/Library/Logs/CopyVerified/copyverified.log
	rm ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	echo
	viewlog
	}

twocopy ()
	{
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo `date` >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " Data Manager: $DMNAME" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "       Source: $SOURCEPATH" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 1: $COPYPATH1" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 2: $COPYPATH2" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "----------------------------------------------------------------------" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	date
	copy2
	check_dest1 &
	check_dest2 &
	check_dest2_comp
	wait
	compa2
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/
	echo
	echo TRANSFERS and MD5 CHECKSUMS COMPLETE
	date
	echo "`date | awk '{print $4}'` Copies and Checksums Sucessful" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH1"/"$DESTPATH"-copy1_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH2"/"$DESTPATH"-copy2_log.txt
	cat ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log >> ~/Library/Logs/CopyVerified/copyverified.log
	rm ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	echo
	viewlog
	}

threecopy ()
	{
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo `date` >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " Data Manager: $DMNAME" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "       Source: $SOURCEPATH" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 1: $COPYPATH1" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 2: $COPYPATH2" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 3: $COPYPATH3" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "----------------------------------------------------------------------" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	date
	copy3
	check_dest1 &
	check_dest2 &
	check_dest3 &
	check_dest3_comp
	wait
	compa3
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH3"/
	echo
	echo TRANSFERS and MD5 CHECKSUMS COMPLETE
	date
	echo "`date | awk '{print $4}'` Copies and Checksums Sucessful" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH1"/"$DESTPATH"-copy1_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH2"/"$DESTPATH"-copy2_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH3"/"$DESTPATH"-copy3_log.txt
	cat ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log >> ~/Library/Logs/CopyVerified/copyverified.log
	rm ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	echo
	viewlog
	}

fourcopy ()
	{
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo `date` >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " Data Manager: $DMNAME" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "       Source: $SOURCEPATH" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 1: $COPYPATH1" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 2: $COPYPATH2" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 3: $COPYPATH3" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "Destination 4: $COPYPATH4" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "----------------------------------------------------------------------" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	date
	copy4
	check_dest1 &
	check_dest2 &
	check_dest3 &
	check_dest4 &
	check_dest4_comp
	wait
	compa4
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH3"/
	cp "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH4"/
	echo
	echo TRANSFERS and MD5 CHECKSUMS COMPLETE
	date
	echo "`date | awk '{print $4}'` Copies and Checksums Sucessful" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo " " >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH1"/"$DESTPATH"-copy1_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH2"/"$DESTPATH"-copy2_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH3"/"$DESTPATH"-copy3_log.txt
	cp ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log "$COPYPATH4"/"$DESTPATH"-copy4_log.txt
	cat ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log >> ~/Library/Logs/CopyVerified/copyverified.log
	rm ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	echo
	viewlog
	}

# This section of the script defines the copy events
# added the -n option to prevent from accidentally overwriting files
copy1 ()
	{
	measure1 &
	echo "`date | awk '{print $4}'` Copying $SOURCEPATH to $COPYPATH1/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp -Rpn "$SOURCEPATH" "$COPYPATH1"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	wait
	}

copy2 ()
	{
	measure1 &
	echo "`date | awk '{print $4}'` Copying $SOURCEPATH to $COPYPATH1/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp -Rpn "$SOURCEPATH" "$COPYPATH1"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	wait
	measure2 &
	check_source &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH2/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH2"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	wait
	}

copy3 ()
	{
	measure1 &
	echo "`date | awk '{print $4}'` Copying $SOURCEPATH to $COPYPATH1/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp -Rpn "$SOURCEPATH" "$COPYPATH1"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	wait
	measure3 &
	check_source &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH2/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH2"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH3/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH3"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	wait
	}

copy4 ()
	{
	measure1 &
	echo "`date | awk '{print $4}'` Copying $SOURCEPATH to $COPYPATH1/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	cp -Rpn "$SOURCEPATH" "$COPYPATH1"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	wait
	measure4 &
	check_source &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH2/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH2"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH3/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH3"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	echo "`date | awk '{print $4}'` Copying $COPYPATH1/$DESTPATH to $COPYPATH4/" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log &
	cp -Rpn "$COPYPATH1"/"$DESTPATH" "$COPYPATH4"/ 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	wait
	}

# This section of the script checks percent complete destination checksums

check_dest1_comp ()
	{
	echo
	check_dest4_complete=0
	sleep 1
	while [ $check_dest4_complete -le 95 ]
	do
		check_dest1=`cat ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp`
		check_dest4_complete=$(echo "$check_dest1" ) ;		
		echo -n -e "\rDESTINATION CHECKSUMS $check_dest4_complete%" COMPLETE
		sleep 2
	done
	echo -n -e "\rDESTINATION CHECKSUMS 100% COMPLETE"
	rm ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp
	}

check_dest2_comp ()
	{
	echo
	check_dest4_complete=0
	sleep 1
	while [ $check_dest4_complete -le 95 ]
	do
		check_dest1=`cat ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp`
		check_dest2=`cat ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp`
		check_dest4_complete=$(echo "($check_dest1 + $check_dest2) / 2 " | bc ) ;		
		echo -n -e "\rDESTINATION CHECKSUMS $check_dest4_complete%" COMPLETE
		sleep 2
	done
	echo -n -e "\rDESTINATION CHECKSUMS 100% COMPLETE"
	rm ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp
	}

check_dest3_comp ()
	{
	echo
	check_dest4_complete=0
	sleep 1
	while [ $check_dest4_complete -le 95 ]
	do
		check_dest1=`cat ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp`
		check_dest2=`cat ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp`
		check_dest3=`cat ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp`
		check_dest4_complete=$(echo "($check_dest1 + $check_dest2 + $check_dest3) / 3 " | bc ) ;		
		echo -n -e "\rDESTINATION CHECKSUMS $check_dest4_complete%" COMPLETE
		sleep 2
	done
	echo -n -e "\rDESTINATION CHECKSUMS 100% COMPLETE"
	rm ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp
	}

check_dest4_comp ()
	{
	echo
	check_dest4_complete=0
	sleep 1
	while [ $check_dest4_complete -le 95 ]
	do
		check_dest1=`cat ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp`
		check_dest2=`cat ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp`
		check_dest3=`cat ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp`
		check_dest4=`cat ~/Library/Logs/CopyVerified/check_dest4_"$DESTPATH".tmp`
		check_dest4_complete=$(echo "($check_dest1 + $check_dest2 + $check_dest3 + $check_dest4) / 4 " | bc ) ;		
		echo -n -e "\rDESTINATION CHECKSUMS $check_dest4_complete%" COMPLETE
		sleep 2
	done
	echo -n -e "\rDESTINATION CHECKSUMS 100%" COMPLETE
	rm ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp
	rm ~/Library/Logs/CopyVerified/check_dest4_"$DESTPATH".tmp
	}


# This section of the script checks percent complete of the copies
measure1 ()
	{
	echo
	var4=0
	var1=`du -s "$SOURCEPATH" | awk '{print $1}'` ;
	sleep 1
	while [ $var4 -le 85 ]
	do
		var2=`du -s "$COPYPATH1"/"$DESTPATH" | awk '{print $1}'` ;
		var3=$(echo "scale=2; ($var2 / $var1) * 100" | bc)
		var4=`echo $var3 | awk -F . '{print $1}'`;
		echo -n -e "\rFIRST COPY $var4%" COMPLETE
		sleep 2
	done
	echo -n -e "\rFIRST COPY 100% COMPLETE"
	}

measure2 ()
	{
	echo
	var4=0
	var1=`du -s "$SOURCEPATH" | awk '{print $1}'` ;
	sleep 1
	while [ $var4 -le 85 ]
	do
		var2=`du -s "$COPYPATH2"/"$DESTPATH" | awk '{print $1}'` ;
		var3=$(echo "scale=2; ($var2 / $var1) * 100" | bc)
		var4=`echo $var3 | awk -F . '{print $1}'`;
		echo -n -e "\rSECOND COPY $var4%" COMPLETE
		sleep 2
	done
	echo -n -e "\rSECOND COPY 100%" COMPLETE
	}

measure3 ()
	{
	echo
	var5=0
	var1=`du -s "$COPYPATH1"/"$DESTPATH" | awk '{print $1}'` ;
	sleep 1
	while [ $var5 -le 85 ]
	do
		var2=`du -s "$COPYPATH2"/"$DESTPATH" | awk '{print $1}'` ;
		var3=`du -s "$COPYPATH3"/"$DESTPATH" | awk '{print $1}'` ;
		var4=$(echo "scale=2; (($var2 + $var3) / ($var1 * 2)) * 100" | bc)
		var5=`echo $var4 | awk -F . '{print $1}'`;
		echo -n -e "\rSECOND and THIRD COPY $var5%" COMPLETE
		sleep 2 ;
	done
	echo -n -e "\rSECOND and THIRD COPY 100%" COMPLETE
	}

measure4 ()
	{
	echo
	var6=0
	var1=`du -s "$COPYPATH1"/"$DESTPATH" | awk '{print $1}'` ;
	sleep 1
	while [ $var6 -le 85 ]
	do
		var2=`du -s "$COPYPATH2"/"$DESTPATH" | awk '{print $1}'` ;
		var3=`du -s "$COPYPATH3"/"$DESTPATH" | awk '{print $1}'` ;
		var4=`du -s "$COPYPATH4"/"$DESTPATH" | awk '{print $1}'` ;
		var5=$(echo "scale=2; (($var2 + $var3 + $var4) / ($var1 * 3)) * 100" | bc) ;
		var6=`echo $var5 | awk -F . '{print $1}'`;
		echo -n -e "\rSECOND, THIRD, and FOURTH COPY $var6%" COMPLETE ;
		sleep 2 ;
	done
	echo -n -e "\rSECOND, THIRD, and FOURTH COPY 100%" COMPLETE
	}


# This section of the script defines doing checksums of the files
check_source ()
	{
	touch "$COPYPATH1"/"$DESTPATH"-source$MD5TXT ;
	cd "$SOURCEPATH"/ ;
#   the change was made to accomodate path/file names with spaces
#	for FILE in $(find -s * -type f 2>&1) ;
	find -s * -type f  | while read FILE
	do  

	chksum0=$(md5 -r "$FILE" 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ) ;
	chksum01=`echo "$chksum0" | awk '{print $1}'` ;
	echo "`date | awk '{print $4}'`  $chksum01  MD5 Checksum on $SOURCEPATH/$FILE" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "$chksum0" >> "$COPYPATH1"/"$DESTPATH"-source$MD5TXT ;

	done
	echo -e "\rSOURCE CHECKSUMS 100% COMPLETE                    " ;
	}

check_dest1 ()
	{
	echo 0 > ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp	;
	filesize0=0
	touch "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT ;
	cd "$COPYPATH1"/"$DESTPATH" ;

    find -s * -type f  | while read FILE1
	do
	chksum1=$(md5 -r "$FILE1" 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ) ;
	chksum11=`echo "$chksum1" | awk '{print $1}'` ;
	echo "`date | awk '{print $4}'`  $chksum11  MD5 Checksum on $COPYPATH1/$DESTPATH/$FILE1" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "$chksum1" >> "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT ;
	var1=`du -s . | awk '{print $1}'` ;
	filesize1=`du -s "$FILE1" | awk '{print $1}'` ;
	filesize0=$((filesize0 + filesize1)) ;
	filesize3=$(echo "scale=2; ($filesize0 / $var1) * 100" | bc) ;
	filesize4=`echo $filesize3 | awk -F . '{print $1}'` ;
	echo $filesize4 > ~/Library/Logs/CopyVerified/check_dest1_"$DESTPATH".tmp	;
	done
	}

check_dest2 ()
	{
	echo 0 > ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp	;
	filesize0=0
	touch "$COPYPATH2"/"$DESTPATH"-copy2$MD5TXT ;
	cd "$COPYPATH2"/"$DESTPATH" ;

	find -s * -type f  | while read FILE2
	do
	chksum2=$(md5 -r "$FILE2" 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ) ;
	chksum21=`echo "$chksum2" | awk '{print $1}'` ;
	echo "`date | awk '{print $4}'`  $chksum21  MD5 Checksum on "$COPYPATH2"/$DESTPATH/$FILE2" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "$chksum2" >> "$COPYPATH2"/"$DESTPATH"-copy2$MD5TXT ;
	var1=`du -s . | awk '{print $1}'` ;
	filesize1=`du -s "$FILE2" | awk '{print $1}'` ;
	filesize0=$((filesize0 + filesize1)) ;
	filesize3=$(echo "scale=2; ($filesize0 / $var1) * 100" | bc) ;
	filesize4=`echo $filesize3 | awk -F . '{print $1}'` ;
	echo $filesize4 > ~/Library/Logs/CopyVerified/check_dest2_"$DESTPATH".tmp	;
	done
	}

check_dest3 ()
	{
	echo 0 > ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp	;
	filesize0=0
	touch "$COPYPATH3"/"$DESTPATH"-copy3$MD5TXT ;
	cd "$COPYPATH3"/"$DESTPATH" ;

    find -s * -type f  | while read FILE3
	do
	chksum3=$(md5 -r "$FILE3" 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ) ;
	chksum31=`echo "$chksum3" | awk '{print $1}'` ;
	echo "`date | awk '{print $4}'`  $chksum31  MD5 Checksum on "$COPYPATH3"/"$DESTPATH"/$FILE3" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "$chksum3" >> "$COPYPATH3"/"$DESTPATH"-copy3$MD5TXT ;
	var1=`du -s . | awk '{print $1}'` ;
	filesize1=`du -s "$FILE3" | awk '{print $1}'` ;
	filesize0=$((filesize0 + filesize1)) ;
	filesize3=$(echo "scale=2; ($filesize0 / $var1) * 100" | bc) ;
	filesize4=`echo $filesize3 | awk -F . '{print $1}'` ;
	echo $filesize4 > ~/Library/Logs/CopyVerified/check_dest3_"$DESTPATH".tmp	;
	done
	}

check_dest4 ()
	{
	echo 0 > ~/Library/Logs/CopyVerified/check_dest4_"$DESTPATH".tmp	;
	filesize0=0
	touch "$COPYPATH4"/"$DESTPATH"-copy4$MD5TXT ;
	cd "$COPYPATH4"/"$DESTPATH" ;

    find -s * -type f  | while read FILE4
	do
	chksum4=$(md5 -r "$FILE4" 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ) ;
	chksum41=`echo "$chksum4" | awk '{print $1}'` ;
	echo "`date | awk '{print $4}'`  $chksum41  MD5 Checksum on "$COPYPATH4"/"$DESTPATH"/$FILE4" >> ~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log ;
	echo "$chksum4" >> "$COPYPATH4"/"$DESTPATH"-copy4$MD5TXT ;
	var1=`du -s . | awk '{print $1}'` ;
	filesize1=`du -s "$FILE4" | awk '{print $1}'` ;
	filesize0=$((filesize0 + filesize1)) ;
	filesize3=$(echo "scale=2; ($filesize0 / $var1) * 100" | bc) ;
	filesize4=`echo $filesize3 | awk -F . '{print $1}'` ;
	echo $filesize4 > ~/Library/Logs/CopyVerified/check_dest4_"$DESTPATH".tmp	;
	done
	}

# This section of the script compares the checksums
compa1 ()
	{
	echo
	echo COMPARING CHECKSUMS
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	}

compa2 ()
	{
	echo
	echo COMPARING CHECKSUMS
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/"$DESTPATH"-copy2$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	}

compa3 ()
	{
	echo
	echo COMPARING CHECKSUMS
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/"$DESTPATH"-copy2$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH3"/"$DESTPATH"-copy3$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	}

compa4 ()
	{
	echo
	echo COMPARING CHECKSUMS
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH1"/"$DESTPATH"-copy1$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH2"/"$DESTPATH"-copy2$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH3"/"$DESTPATH"-copy3$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	diff "$COPYPATH1"/"$DESTPATH"-source$MD5TXT "$COPYPATH4"/"$DESTPATH"-copy4$MD5TXT 2>>~/Library/Logs/CopyVerified/copyverified_"$DESTPATH".log
	}


# This section of the script reads the config file
# fields are comma separated and added: BEGIN { FS = "," } to deal with spaces in path names
readone ()
{
	dCOPYPATH1=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $2}'`;
	dDMNAME=`cat ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config`;
}

readtwo ()
{
	dCOPYPATH1=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $2}'`;
	dCOPYPATH2=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $3}'`;
	dDMNAME=`cat ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config`;
}

readthree ()
{
	dCOPYPATH1=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $2}'`;
	dCOPYPATH2=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $3}'`;
	dCOPYPATH3=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $4}'`;
	dDMNAME=`cat ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config`;

}

readfour ()
{
	dCOPYPATH1=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $2}'`;
	dCOPYPATH2=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $3}'`;
	dCOPYPATH3=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $4}'`;
	dCOPYPATH4=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $5}'`;
	dDMNAME=`cat ~/Library/Logs/CopyVerified/copyverifiedDMNAME.config`;
}

viewlog ()
{
	if [ $VIEWLOG1 -eq 1 ]
	then 
		open ~/Library/Logs/CopyVerified/copyverified.log ;
	fi
}

readconfig ()
{
# This section of the script checks for an existing config file
# and reads the values as defaults into the Pashua dialog
clear
if [ -f ~/Library/Logs/CopyVerified/copyverified.config ]
then file1=`cat ~/Library/Logs/CopyVerified/copyverified.config | awk 'BEGIN { FS = "," } {print $1}'`
    dNUM_COPIES=$file1
    if [ $file1 -eq 1 ]
    then readone
    else
        if [ $file1 -eq 2 ]
        then readtwo
        else
            if  [ $file1 -eq 3 ]
            then readthree
            else
                if [ $file1 -eq 4 ]
                then readfour
                fi
            fi
        fi
    fi
else
    dNUM_COPIES="1"
	if [ ! -d ~/Library/Logs/CopyVerified ]
	then
	    mkdir ~/Library/Logs/CopyVerified ;
	fi
fi
}

validate_user_inputs () {
cancel="false"
retry="false"
if [ $CANCELBUTTON -eq 1 ]
then
    cancel="true"
else
    # Check for missing information
    if [ -z "$DMNAME" ]
    then
        echo "You missed entering the Data Manager's Name!"
        echo ""
        retry="true"
    fi
    # determine the intention of the user as to the number of copies by
    # looking at the highest selected COPYPATH
    if [ ! -z "$COPYPATH4" ]
    then
        NUM_COPIES=4
    else
        if [ ! -z "$COPYPATH3" ]
        then
            NUM_COPIES=3
        else
            if [ ! -z "$COPYPATH2" ]
            then
                NUM_COPIES=2
            else
                if [ ! -z "$COPYPATH1" ]
                then
                    NUM_COPIES=1
                else
                    echo "Please choose at least one destination folder!"
                    retry="true"
                fi
            fi
        fi
    fi

    if [[ $NUM_COPIES -ge 1 && -z "$COPYPATH1" || $NUM_COPIES -ge 1 && ! -d "$COPYPATH1" ]]
    then
        echo "Error in selection of the first destination folder!"
        echo -e "$COPYPATH1\n"
        retry="true"
    fi
    if [[ $NUM_COPIES -ge 2 && -z "$COPYPATH2" || $NUM_COPIES -ge 2 && ! -d "$COPYPATH2" ]]
    then
        echo "Error in selection of the second destination folder!"
        echo -e "$COPYPATH2\n"
        retry="true"
    fi
    if [[ $NUM_COPIES -ge 3 && -z "$COPYPATH3" || $NUM_COPIES -ge 3 && ! -d "$COPYPATH3" ]]
    then
        echo "Error in selection of the third destination folder!"
        echo -e "$COPYPATH3\n"
        retry="true"
    fi
    if [[ $NUM_COPIES -ge 4 && -z "$COPYPATH4" || $NUM_COPIES -ge 4 && ! -d "$COPYPATH4" ]]
    then
        echo "Error in selection of the fourth destination folder!"
        echo -e "$COPYPATH4\n"
        retry="true"
    fi

    if [[ -z "$SOURCEPATH" || ! -d  "$SOURCEPATH" ]]
    then
        echo "Error in selection of the source folder!"
        echo -e "$SOURCEPATH\n"
        retry="true"
    fi
fi
# display the error message for a few seconds before showing the dialog again
if  [ $retry == "true" ]
then
    echo "*** Please correct your inputs ***"
    sleep 3
fi
}

#####################################
### Script starts to execute here ###
#####################################
echo -e "CopyVerified $VERSION\n"

continue="true"
# GUI loop until user exits by clicking Cancel
while [ $continue == "true" ]; do

    readconfig
    #Define what the Pashua GUI dialog should be like
    input1="
    # Set transparency: 0 is transparent, 1 is opaque
    *.transparency=0.95

    # Set window title
    *.title = CopyVerified Input

    # Introductory text
    tb.type = text
    tb.default = CopyVerified is a simple app designed primarily to make reliable copies of media from flash memory cards. It will copy a source folder to up to four destinations and will create MD5 checksums for all files in source and destinations to ensure your copies match exactly. It will not overwrite existing files! (beta code - use at your own risk!)

    tb.height = 100
    tb.width = 300
    tb.x = 250
    tb.y = 400

    ## Select number of copies
    #NUM_COPIES.type = popup
    #NUM_COPIES.label = How many copies?
    #NUM_COPIES.width = 60
    #NUM_COPIES.option = 1
    #NUM_COPIES.option = 2
    #NUM_COPIES.option = 3
    #NUM_COPIES.option = 4
    #NUM_COPIES.default = $dNUM_COPIES

    DMNAME.type = textfield
    DMNAME.label = What is the Data Manager's name?
    DMNAME.default = eWrangler
    DMNAME.width = 100
    DMNAME.default =$dDMNAME

    # Add a filesystem browser
    COPYPATH1.type = openbrowser
    COPYPATH1.label = Where should I COPY the files to FIRST:
    COPYPATH1.width=400
    COPYPATH1.default = $dCOPYPATH1

    COPYPATH2.type = openbrowser
    COPYPATH2.label = Where should I COPY the files to SECOND:
    COPYPATH2.width=400
    COPYPATH2.default = $dCOPYPATH2

    COPYPATH3.type = openbrowser
    COPYPATH3.label = Where should I COPY the files to THIRD:
    COPYPATH3.width=400
    COPYPATH3.default = $dCOPYPATH3

    COPYPATH4.type = openbrowser
    COPYPATH4.label = Where should I COPY the files to FOURTH:
    COPYPATH4.width=400
    COPYPATH4.default = $dCOPYPATH4

    # Add a filesystem browser
    SOURCEPATH.type = openbrowser
    SOURCEPATH.label = Where are the SOURCE files located:
    SOURCEPATH.width=400

    # Add a checkbox
    VIEWLOG1.type = checkbox
    VIEWLOG1.label = Show the log file when finished
    VIEWLOG1.rely = -18
    VIEWLOG1.default = 0

    # Add a cancel button with default label
    CANCELBUTTON.type=cancelbutton
    "

    # GUI loop until all user input is OK
    retry="true"
    while [ $retry == "true" ]; do
        # Display the dialog
        pashua_run "$input1"
        validate_user_inputs
    done

    if [ $cancel == "true" ]
    then
        echo "*** *** *** EXIT *** *** ***"
        exit
    else
        # Save configuration and start copying
        saveconfig_thencopy
        retry="true"
        echo "*** *** *** *** *** *** *** ***"
        sleep 3
    fi
done
